#include <iostream>
#include <string.h>
#include <algorithm>
#include <regex>
#include <cmath>
//get everything from libaries

using namespace std;



// converts character array
// to string and returns it
string convertToString(char* a, int size)
{
    int i;
    string s = "";
    for (i = 0; i < size; i++) {
        s = s + a[i];
    }
    return s;
}


string trim(string str){
    return regex_replace(str, regex("(^[ ]+)|([ ]+$)"),"");
}


char findOperator(string equation){
    if (equation.find("+") !=std::string::npos )
        return '+';
    else if (equation.find("-") !=std::string::npos )
        return '-';
    else if (equation.find("*") !=std::string::npos)
        return '*';
    else if (equation.find("/") !=std::string::npos)
        return '/';
    else if (equation.find("^") !=std::string::npos)   
        return '^';
}


float cal(string equation, char oper){
    int operator_idx;
    string a, b, a_temp, b_temp;
    float a_f, b_f;

    operator_idx = equation.find(oper);
 

    cout << operator_idx<<endl;

    a = equation.substr(0, operator_idx);
    b = equation.substr(operator_idx+1, -1);

    a_temp = trim(a);
    b_temp = trim(b);

    a_f = stof(a_temp); 
    b_f = stof(b_temp);

    cout <<"**checking a_temp, b_temp" << endl;
    cout << a_temp << endl;
    cout << b_temp << endl;


    // return 1;
    switch(oper){
        case '+':
            return a_f + b_f;
        case '-':
            return a_f - b_f;
        case '*':
            return a_f * b_f;
        case '/':
            return a_f / b_f;
        case '^':
            return pow(a_f , b_f);
    }
}


int main()
{
    char name[100]={0};
    char oper;
    int operator_idx;

    string equation;

    float cal_result;

    cin.getline(name,100); 
    cout << name;

    equation  = convertToString(name, 100);

    cout << equation << endl << endl;

    oper = findOperator(equation);
    cout << "operator is:" << oper << endl;
    cal_result = cal(equation,oper);

    cout << cal_result << endl;
}

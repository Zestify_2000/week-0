#include <iostream>
#include <string.h>
#include <fstream>

using namespace std;

string getInputSequence(char * filename){
    string line;
    ifstream myfile (filename);

    if (myfile.is_open()){
        while ( myfile.good() ){
            getline (myfile,line);
            cout << line << endl;
        }
        myfile.close();
    }
    else{
        cout << "Unable to open file"; 
    }
    return line;
}

float calActive(char * filename){
    int can_track=0;
    int status[4000] ={0};
    string line = getInputSequence(filename);
    int line_len = line.size();
    int awake_sum=0;
    // ifstream myfile (filename);

    for (int i = 0; i < line.size(); i++){
        cout << line[i] << endl;
        // nothing (no vending machine or break room)
        if(line[i] == 'X'){
            if (can_track > 0){
                status[i] = 1;
                can_track = can_track-1;
            }
        }
        // break room only
        else if(line[i] == 'N'){
            status[i] = 1;
        }
        // vending machine
        else if(line[i] == 'V'){
            if (can_track < 2)
                status[i] = 1;
                can_track = can_track+1;
        }
        // vending machine + break room
        else if(line[i] == 'B'){
            status[i] = 1;
            can_track = 2;
        }
        else{}
        // cout << status[i] << " " << can_track << endl;
    }
    //total awake classes is added 
    for (int i=0; i<line_len; i++){
        awake_sum += status[i];
    }
    
    return float(awake_sum)/line_len;
}

void outputResult(char * filename, float ratio){
    string outfilename =  string(filename) + ".out";
    cout <<outfilename;

    ofstream ofs(outfilename) ;
    if( ! ofs )	{
        cout << "Error opening file for output" << endl ;
    } 	
    ofs << ratio << endl ;
    ofs.close() ;
}

int main(int argc, char *argv[]){
    float ratio=0;
    const char *ext =".txt" ;
    size_t xlen = strlen(ext);
    for (int i=1; i<argc; i++){
        size_t slen = strlen(argv[i]) ;
        if(strcmp(argv[i]+slen-xlen,ext)==0){
            cout<<argv[i]<<endl;
            ratio = calActive(argv[i]);

            cout << ratio;

            outputResult(argv[i], ratio);

        }
        else
            cout<<"no match"<<endl;
    }


    return 0;
}

// /usr/bin/g++ -fdiagnostics-color=always -g /home/royce/Desktop/project/fizzbuzzbaz/alexaddiction.cpp -o /home/royce/Desktop/project/fizzbuzzbaz/alexaddiction

// char findOperator(string equation){
//     if (equation.find("X") !=std::string::npos )
//         return 'X';
//     else if (equation.find("V") !=std::string::npos )
//         return 'V';
//     else if (equation.find("N") !=std::string::npos)
//         return 'N';
//     else if (equation.find("B") !=std::string::npos)
//         return 'B';
// }

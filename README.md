# Week 0: Welcome & Setup

Welcome to your first (of many!) Git repositories on this team. Make sure that you check out the introduction post first: http://frc4739.freeddns.org/software/intro/week-0.html

This repo contains a C++ "Hello, World!" example program that you should try to compile run. You should create a new file in this repository with some contents (up to you, surprise me) to try out creating and pushing commits.


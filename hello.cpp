// Import the IO library, which allows us to read from and write to the terminal
#include <iostream>

// Declare the main function, which will be called when we run the program
// `int` indicates that it returns an integer value
// C++ is a strongly typed language so we must declare the type of everything
// `()` indicates that it takes no arguments
// Everything between the curly braces {} represents the function code
int main() {
    // The `std::` prefix means you are using something from the standard library (namespace)
    // `std::cout`: console output
    // `<<`: Insertion operator, shoves the content on the right into the stream on the left
    // In this case, it outputs "Hello, World!" onto the console
    // `std::endl`: new line (also flushes the buffer, forcing the text to be displayed immediately)
    // `;`: Every line in C++ must end with a semicolon
    std::cout << "Hello, World!" << std::endl;
    // Return an exit code of 0
    // Indicates that the program ran to completion without any errors
    // Non-zero exit codes can be used to show failure
    // This is a convention from Unix
    // If you omit this line, zero is implicitly returned from main
    return 0;
}


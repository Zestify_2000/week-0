#include <iostream>
#include <cmath>
using namespace std;


float euclideanD(float x1, float y1, float x2, float y2){
    return sqrt(pow((x2-x1),2) + pow((y2-y1),2) );
}


void symCal(int arr[4000][2], int len){
    //sym around x axis
    for (int i=0; i<len; i++){
        arr[i+len][0] = arr[i][0]; 
        arr[i+len][1] = 0-arr[i][1];
    }
    //sym around y axis
    for (int i=0; i<len*2; i++){
        arr[i+len*2][0] = 0-arr[i][0];
        arr[i+len*2][1] = arr[i][1];
    }
}

// in, in_len, out, out_len, radius
void axisCal(int in[4000][2], int in_len, int out[4000][2], int out_len, float radius){
    int ring_out = ceil(radius);
    int ring_in = floor(radius);
    in[in_len][0] = ring_in;
    in[in_len][1] = 0;
    in_len++;
    in[in_len][0] = 0-ring_in;
    in[in_len][1] = 0;
    in_len++;
    in[in_len][0] = 0;
    in[in_len][1] = ring_in;
    in_len++;
    in[in_len][0] = 0;
    in[in_len][1] = 0-ring_in;
    in_len++;

    out[out_len][0] = ring_out;
    out[out_len][1] = 0;
    out_len++;
    out[out_len][0] = 0-ring_out;
    out[out_len][1] = 0;
    out_len++;
    out[out_len][0] = 0;
    out[out_len][1] = ring_out;
    out_len++;
    out[out_len][0] = 0;
    out[out_len][1] = 0-ring_out;
    out_len++;
}



void output(int arr[4000][2], int len){
    for (int i=0;i<len;i++){
        cout << "(" << arr[i][0] << "," << arr[i][1] << ")" << endl;
    }
}

int main()
{
    float radius = 4.01, distance = -1.0;
    bool inside_status = true;
    int ring_out = ceil(radius);
    int out[4000][2], in[4000][2]; 
    int out_len=0, in_len=0;

    // horizontal base point
    for (int i = 1; i <=ring_out; ++i) {
        // vertical point check
        int j=1;
        inside_status = true;
        for (; j <=ring_out ; j++){
            distance = euclideanD(0,0,i,j);
            if (distance > radius){
                inside_status = false;
                break;
            }
        }
        // if existint point go outside 
        if (inside_status==false  && out[out_len-1][1]!=1 ){            
            out[out_len][0]=i;
            out[out_len][1]=j;
            in[in_len][0]=i;
            in[in_len][1]=j-1;
            out_len++;
            in_len++;
        }
        // if in array have point on x axis remove it 
        // prepare for symmytry operation
        if(in[in_len-1][1]==0){
            in_len-=1;
        }
    }
    // symmetry operation
    symCal(out, out_len);
    out_len*=4;
    symCal(in, in_len);
    in_len*=4;

    //add point in axis for in and out
    axisCal(in, in_len, out, out_len, radius);
    in_len+=4;
    out_len+=4;    // cout << distance;


    cout << "****radius "<<radius <<" ****" <<endl;

    cout << "****out point****" <<endl;
    output(out, out_len);

    cout << "****in point****" <<endl;
    output(in, in_len);

    return 0;
}


#include <iostream>
using namespace std;

int main()
{
    int n = 1000;

    for (int i = 1; i <= n; ++i) {
        if (i%3!=0 && i%5!=0 && i%7!=0) {
            std::cout << i << " "<<std::endl;
        }
        else{
            if (i%3==0 && i%5==0 && i%7==0){
                cout << "fizzbuzzbaz" << endl;
            }
            else if (i%3==0 && i%5==0){
                cout << "fizzbuzz" << endl;
            }
            else if( i%3==0 && i%5!=0){
                cout << "fizz" << endl;
            }
            else if( i%3!=0 && i%5==0){
                cout << "buzz" << endl;
            }
            else if( i%7==0){
                cout << "baz" << endl;
            }
        }
    }
    return 0;
}